import os
import discord
from discord.ext import commands
import json
import random

TOKEN = os.getenv('NzU2NTA0Mzg4ODc2MzcwMDQy.X2SznQ.VhdkcKZrDcz46tZpGEzHDsU9OtM')
GUILD = os.getenv('DISCORD_GUILD')
client = commands.Bot(command_prefix = "!")


@client.event
async def on_ready():
    for guild in client.guilds:
        print("Logged in as")
        print(client.user.name)
        print(client.user.id)
        print('------')
        print("Current Guild")
        print(guild.name)
        print(guild.id)
        print('------')

@client.command()
async def balance(ctx):
    await open_account(ctx.author)
    user = ctx.author
    users = await get_bank_data()

    Wallet_amt = users[str(user.id)]["Wallet"]
    Bank_amt = users[str(user.id)]["Bank"]

    em = discord.Embed(title = f"{ctx.author.name}'s balance'", color = discord.Color.red())
    em.add_field(name = "Wallet balance",value = Wallet_amt)
    em.add_field(name = "Bank balance",value = Bank_amt)
    await ctx.send(embed = em)

@client.command()
async def beg(ctx):
    await open_account(ctx.author)

    users = await get_bank_data()

    earnings = random.randrange(11)

    await ctx.send("Someone has given you {earnings} coins.")

    users[str(user.id)]["Wallet"] += earnings

    with open("mainbank.json", "w") as f:
        json.dump(users,f)

async def open_account(user):

    users = await get_bank_data()
    user = ctx.author
    if str(user.id) in users:
        return False
    else:
        users[str(user.id)] = {}
        users[str(user.id)]["Wallet"] = 0
        users[str(user.id)]["Bank"] = 0

    with open("mainbank.json", "w") as f:
        json.dump(users,f)
    return True


async def get_bank_data():
    with open("mainbank.json", "r") as f:
        users = json.load(f)

    return users


client.run("Token")
